# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.85.0"
  constraints = "~> 3.0"
  hashes = [
    "h1:wwEB+rR+nTi4XeOPiCpqyjfbAEgIsxjbvkH6WuUcYcU=",
  ]
}

provider "registry.terraform.io/kreuzwerker/docker" {
  version     = "2.11.0"
  constraints = "~> 2.11.0"
  hashes = [
    "h1:Qzz/xfsrolaSd7Qnxk330hjmCiHqy/n0C2T3k58EDDg=",
  ]
}
