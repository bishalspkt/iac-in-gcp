"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cdktf_1 = require("cdktf");
const docker_provider_1 = require("./.gen/providers/docker/docker-provider");
const kubernetes_provider_1 = require("./.gen/providers/kubernetes/kubernetes-provider");
const config_1 = require("./config");
const auth_1 = require("./.gen/modules/terraform-google-modules/kubernetes-engine/google/modules/auth");
const resource_1 = require("./.gen/providers/null/resource");
const saCred = require("./secrets/terraform-sa-key.json");
const provider_google_1 = require("@cdktf/provider-google");
// https://developers.google.com/identity/protocols/oauth2/scopes
const oauthScopes = [
    "https://www.googleapis.com/auth/devstorage.read_only",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring",
    "https://www.googleapis.com/auth/servicecontrol",
    "https://www.googleapis.com/auth/service.management.readonly",
    "https://www.googleapis.com/auth/trace.append",
    "https://www.googleapis.com/auth/cloud-platform",
];
class KubernetesCluster extends resource_1.Resource {
    constructor(scope, name, serviceAccount) {
        super(scope, name);
        this.sa = serviceAccount;
        this.cluster = new provider_google_1.ContainerCluster(this, "cluster", {
            name,
            removeDefaultNodePool: true,
            initialNodeCount: 1,
            nodeConfig: [
                {
                    preemptible: true,
                    serviceAccount: this.sa.email,
                    oauthScopes,
                },
            ],
        });
    }
    addNodePool(name, nodeCount = 3, machineType = "e2-medium") {
        new provider_google_1.ContainerNodePool(this, name, {
            name,
            cluster: this.cluster.name,
            nodeCount,
            nodeConfig: [
                {
                    preemptible: true,
                    machineType,
                    serviceAccount: this.sa.email,
                    oauthScopes,
                },
            ],
        });
    }
    addAutoscalingNodePool(name, minNodeCount = 1, maxNodeCount = 2, machineType = "e2-medium") {
        new provider_google_1.ContainerNodePool(this, name, {
            name,
            cluster: this.cluster.name,
            autoscaling: [
                {
                    minNodeCount,
                    maxNodeCount,
                },
            ],
            nodeConfig: [
                {
                    preemptible: true,
                    machineType,
                    serviceAccount: this.sa.email,
                    oauthScopes,
                },
            ],
        });
    }
    static onCluster(scope, name) {
        const cluster = new provider_google_1.DataGoogleContainerCluster(scope, "cluster", {
            name,
        });
        const auth = new auth_1.TerraformGoogleModulesKubernetesEngineGoogleModulesAuth(scope, "auth", {
            clusterName: cluster.name,
            location: cluster.location,
            projectId: cluster.project,
        });
        new kubernetes_provider_1.KubernetesProvider(scope, "kubernetes", {
            clusterCaCertificate: auth.clusterCaCertificateOutput,
            host: auth.hostOutput,
            token: auth.tokenOutput,
        });
        return {};
    }
}
class InfrastructureLayer extends cdktf_1.TerraformStack {
    constructor(scope, name) {
        super(scope, name);
        new provider_google_1.GoogleProvider(this, "google", {
            zone: "us-central1-c",
            project: "bishal-playground-313602",
            credentials: JSON.stringify(saCred)
        });
        const sa = new provider_google_1.ServiceAccount(this, "sa", {
            accountId: "sensand-terraform-cdk",
            displayName: "sensand-terraform-cdk"
        });
        new provider_google_1.ProjectIamMember(this, "sa-role-binding", {
            role: "roles/storage.admin",
            member: `serviceAccount:${sa.email}`,
        });
        new provider_google_1.ContainerRegistry(this, "registry", {});
        const cluster = new KubernetesCluster(this, config_1.CLUSTER_NAME, sa);
        cluster.addNodePool("main");
        cluster.addAutoscalingNodePool("workloads");
    }
}
class BaselineLayer extends cdktf_1.TerraformStack {
    constructor(scope, name) {
        super(scope, name);
        new provider_google_1.GoogleProvider(this, "google", {
            zone: "us-central1-c",
            project: "bishal-playground-313602",
        });
    }
}
class ApplicationLayer extends cdktf_1.TerraformStack {
    constructor(scope, name) {
        super(scope, name);
        new provider_google_1.GoogleProvider(this, "google", {
            zone: "us-central1-c",
            project: "bishal-playground-313602",
        });
        new docker_provider_1.DockerProvider(this, "docker", {});
    }
}
const app = new cdktf_1.App();
new InfrastructureLayer(app, "infrastructure");
new BaselineLayer(app, "baseline");
new ApplicationLayer(app, "development");
new ApplicationLayer(app, "staging");
new ApplicationLayer(app, "production");
app.synth();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSxpQ0FBNEM7QUFDNUMsNkVBQXlFO0FBQ3pFLHlGQUFxRjtBQUVyRixxQ0FBd0M7QUFDeEMsd0dBQW1LO0FBQ25LLDZEQUEwRDtBQUMxRCwwREFBMEQ7QUFDMUQsNERBUWdDO0FBRWhDLGlFQUFpRTtBQUNqRSxNQUFNLFdBQVcsR0FBRztJQUNsQixzREFBc0Q7SUFDdEQsK0NBQStDO0lBQy9DLDRDQUE0QztJQUM1QyxnREFBZ0Q7SUFDaEQsNkRBQTZEO0lBQzdELDhDQUE4QztJQUM5QyxnREFBZ0Q7Q0FDakQsQ0FBQztBQUVGLE1BQU0saUJBQWtCLFNBQVEsbUJBQVE7SUFJdEMsWUFBWSxLQUFnQixFQUFFLElBQVksRUFBRSxjQUE4QjtRQUN4RSxLQUFLLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRW5CLElBQUksQ0FBQyxFQUFFLEdBQUcsY0FBYyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxrQ0FBZ0IsQ0FBQyxJQUFJLEVBQUUsU0FBUyxFQUFFO1lBQ25ELElBQUk7WUFDSixxQkFBcUIsRUFBRSxJQUFJO1lBQzNCLGdCQUFnQixFQUFFLENBQUM7WUFDbkIsVUFBVSxFQUFFO2dCQUNWO29CQUNFLFdBQVcsRUFBRSxJQUFJO29CQUNqQixjQUFjLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLO29CQUM3QixXQUFXO2lCQUNaO2FBQ0Y7U0FDRixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsV0FBVyxDQUFDLElBQVksRUFBRSxTQUFTLEdBQUcsQ0FBQyxFQUFFLFdBQVcsR0FBRyxXQUFXO1FBQ2hFLElBQUksbUNBQWlCLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRTtZQUNoQyxJQUFJO1lBQ0osT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUMxQixTQUFTO1lBQ1QsVUFBVSxFQUFFO2dCQUNWO29CQUNFLFdBQVcsRUFBRSxJQUFJO29CQUNqQixXQUFXO29CQUNYLGNBQWMsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUs7b0JBQzdCLFdBQVc7aUJBQ1o7YUFDRjtTQUNGLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxzQkFBc0IsQ0FDcEIsSUFBWSxFQUNaLFlBQVksR0FBRyxDQUFDLEVBQ2hCLFlBQVksR0FBRyxDQUFDLEVBQ2hCLFdBQVcsR0FBRyxXQUFXO1FBRXpCLElBQUksbUNBQWlCLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRTtZQUNoQyxJQUFJO1lBQ0osT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUMxQixXQUFXLEVBQUU7Z0JBQ1g7b0JBQ0UsWUFBWTtvQkFDWixZQUFZO2lCQUNiO2FBQ0Y7WUFDRCxVQUFVLEVBQUU7Z0JBQ1Y7b0JBQ0UsV0FBVyxFQUFFLElBQUk7b0JBQ2pCLFdBQVc7b0JBQ1gsY0FBYyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSztvQkFDN0IsV0FBVztpQkFDWjthQUNGO1NBQ0YsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBZ0IsRUFBRSxJQUFZO1FBQzdDLE1BQU0sT0FBTyxHQUFHLElBQUksNENBQTBCLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRTtZQUMvRCxJQUFJO1NBQ0wsQ0FBQyxDQUFDO1FBRUgsTUFBTSxJQUFJLEdBQUcsSUFBSSw4REFBTyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUU7WUFDdEMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxJQUFJO1lBQ3pCLFFBQVEsRUFBRSxPQUFPLENBQUMsUUFBUTtZQUMxQixTQUFTLEVBQUUsT0FBTyxDQUFDLE9BQU87U0FDM0IsQ0FBQyxDQUFDO1FBRUgsSUFBSSx3Q0FBa0IsQ0FBQyxLQUFLLEVBQUUsWUFBWSxFQUFFO1lBQzFDLG9CQUFvQixFQUFFLElBQUksQ0FBQywwQkFBMEI7WUFDckQsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVO1lBQ3JCLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVztTQUN4QixDQUFDLENBQUM7UUFFSCxPQUFPLEVBRU4sQ0FBQztJQUNKLENBQUM7Q0FDRjtBQUVELE1BQU0sbUJBQW9CLFNBQVEsc0JBQWM7SUFDOUMsWUFBWSxLQUFnQixFQUFFLElBQVk7UUFDeEMsS0FBSyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVuQixJQUFJLGdDQUFjLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRTtZQUNqQyxJQUFJLEVBQUUsZUFBZTtZQUNyQixPQUFPLEVBQUUsMEJBQTBCO1lBQ25DLFdBQVcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQztTQUNwQyxDQUFDLENBQUM7UUFFSCxNQUFNLEVBQUUsR0FBRyxJQUFJLGdDQUFjLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRTtZQUN0QyxTQUFTLEVBQUUsdUJBQXVCO1lBQ2xDLFdBQVcsRUFBRSx1QkFBdUI7U0FDdkMsQ0FBRSxDQUFDO1FBR0osSUFBSSxrQ0FBZ0IsQ0FBQyxJQUFJLEVBQUUsaUJBQWlCLEVBQUU7WUFDNUMsSUFBSSxFQUFFLHFCQUFxQjtZQUMzQixNQUFNLEVBQUUsa0JBQWtCLEVBQUUsQ0FBQyxLQUFLLEVBQUU7U0FDckMsQ0FBQyxDQUFDO1FBSUgsSUFBSSxtQ0FBaUIsQ0FBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBRTVDLE1BQU0sT0FBTyxHQUFHLElBQUksaUJBQWlCLENBQUMsSUFBSSxFQUFFLHFCQUFZLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDOUQsT0FBTyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM1QixPQUFPLENBQUMsc0JBQXNCLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDOUMsQ0FBQztDQUNGO0FBRUQsTUFBTSxhQUFjLFNBQVEsc0JBQWM7SUFDeEMsWUFBWSxLQUFnQixFQUFFLElBQVk7UUFDeEMsS0FBSyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNuQixJQUFJLGdDQUFjLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRTtZQUNqQyxJQUFJLEVBQUUsZUFBZTtZQUNyQixPQUFPLEVBQUUsMEJBQTBCO1NBQ3BDLENBQUMsQ0FBQztJQUVMLENBQUM7Q0FDRjtBQUVELE1BQU0sZ0JBQWlCLFNBQVEsc0JBQWM7SUFDM0MsWUFBWSxLQUFnQixFQUFFLElBQVk7UUFDeEMsS0FBSyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNuQixJQUFJLGdDQUFjLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRTtZQUNqQyxJQUFJLEVBQUUsZUFBZTtZQUNyQixPQUFPLEVBQUUsMEJBQTBCO1NBQ3BDLENBQUMsQ0FBQztRQUNILElBQUksZ0NBQWMsQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRXpDLENBQUM7Q0FDRjtBQUVELE1BQU0sR0FBRyxHQUFHLElBQUksV0FBRyxFQUFFLENBQUM7QUFDdEIsSUFBSSxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztBQUMvQyxJQUFJLGFBQWEsQ0FBQyxHQUFHLEVBQUUsVUFBVSxDQUFDLENBQUM7QUFDbkMsSUFBSSxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsYUFBYSxDQUFDLENBQUM7QUFDekMsSUFBSSxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsU0FBUyxDQUFDLENBQUM7QUFDckMsSUFBSSxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsWUFBWSxDQUFDLENBQUM7QUFDeEMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29uc3RydWN0IH0gZnJvbSBcImNvbnN0cnVjdHNcIjtcbmltcG9ydCB7IEFwcCwgVGVycmFmb3JtU3RhY2sgfSBmcm9tIFwiY2RrdGZcIjtcbmltcG9ydCB7IERvY2tlclByb3ZpZGVyIH0gZnJvbSBcIi4vLmdlbi9wcm92aWRlcnMvZG9ja2VyL2RvY2tlci1wcm92aWRlclwiO1xuaW1wb3J0IHsgS3ViZXJuZXRlc1Byb3ZpZGVyIH0gZnJvbSBcIi4vLmdlbi9wcm92aWRlcnMva3ViZXJuZXRlcy9rdWJlcm5ldGVzLXByb3ZpZGVyXCI7XG5cbmltcG9ydCB7IENMVVNURVJfTkFNRSB9IGZyb20gXCIuL2NvbmZpZ1wiO1xuaW1wb3J0IHsgVGVycmFmb3JtR29vZ2xlTW9kdWxlc0t1YmVybmV0ZXNFbmdpbmVHb29nbGVNb2R1bGVzQXV0aCBhcyBHS0VBdXRoIH0gZnJvbSBcIi4vLmdlbi9tb2R1bGVzL3RlcnJhZm9ybS1nb29nbGUtbW9kdWxlcy9rdWJlcm5ldGVzLWVuZ2luZS9nb29nbGUvbW9kdWxlcy9hdXRoXCI7XG5pbXBvcnQgeyBSZXNvdXJjZSB9IGZyb20gXCIuLy5nZW4vcHJvdmlkZXJzL251bGwvcmVzb3VyY2VcIjtcbmltcG9ydCAqIGFzIHNhQ3JlZCBmcm9tIFwiLi9zZWNyZXRzL3RlcnJhZm9ybS1zYS1rZXkuanNvblwiO1xuaW1wb3J0IHtcbiAgQ29udGFpbmVyQ2x1c3RlcixcbiAgQ29udGFpbmVyTm9kZVBvb2wsXG4gIENvbnRhaW5lclJlZ2lzdHJ5LFxuICBEYXRhR29vZ2xlQ29udGFpbmVyQ2x1c3RlcixcbiAgR29vZ2xlUHJvdmlkZXIsXG4gIFByb2plY3RJYW1NZW1iZXIsXG4gIFNlcnZpY2VBY2NvdW50LFxufSBmcm9tIFwiQGNka3RmL3Byb3ZpZGVyLWdvb2dsZVwiO1xuXG4vLyBodHRwczovL2RldmVsb3BlcnMuZ29vZ2xlLmNvbS9pZGVudGl0eS9wcm90b2NvbHMvb2F1dGgyL3Njb3Blc1xuY29uc3Qgb2F1dGhTY29wZXMgPSBbXG4gIFwiaHR0cHM6Ly93d3cuZ29vZ2xlYXBpcy5jb20vYXV0aC9kZXZzdG9yYWdlLnJlYWRfb25seVwiLFxuICBcImh0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL2F1dGgvbG9nZ2luZy53cml0ZVwiLFxuICBcImh0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL2F1dGgvbW9uaXRvcmluZ1wiLFxuICBcImh0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL2F1dGgvc2VydmljZWNvbnRyb2xcIixcbiAgXCJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9hdXRoL3NlcnZpY2UubWFuYWdlbWVudC5yZWFkb25seVwiLFxuICBcImh0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL2F1dGgvdHJhY2UuYXBwZW5kXCIsXG4gIFwiaHR0cHM6Ly93d3cuZ29vZ2xlYXBpcy5jb20vYXV0aC9jbG91ZC1wbGF0Zm9ybVwiLFxuXTtcblxuY2xhc3MgS3ViZXJuZXRlc0NsdXN0ZXIgZXh0ZW5kcyBSZXNvdXJjZSB7XG4gIHByaXZhdGUgc2E6IFNlcnZpY2VBY2NvdW50O1xuICBwcml2YXRlIGNsdXN0ZXI6IENvbnRhaW5lckNsdXN0ZXI7XG5cbiAgY29uc3RydWN0b3Ioc2NvcGU6IENvbnN0cnVjdCwgbmFtZTogc3RyaW5nLCBzZXJ2aWNlQWNjb3VudDogU2VydmljZUFjY291bnQpIHtcbiAgICBzdXBlcihzY29wZSwgbmFtZSk7XG5cbiAgICB0aGlzLnNhID0gc2VydmljZUFjY291bnQ7XG4gICAgdGhpcy5jbHVzdGVyID0gbmV3IENvbnRhaW5lckNsdXN0ZXIodGhpcywgXCJjbHVzdGVyXCIsIHtcbiAgICAgIG5hbWUsXG4gICAgICByZW1vdmVEZWZhdWx0Tm9kZVBvb2w6IHRydWUsXG4gICAgICBpbml0aWFsTm9kZUNvdW50OiAxLFxuICAgICAgbm9kZUNvbmZpZzogW1xuICAgICAgICB7XG4gICAgICAgICAgcHJlZW1wdGlibGU6IHRydWUsXG4gICAgICAgICAgc2VydmljZUFjY291bnQ6IHRoaXMuc2EuZW1haWwsXG4gICAgICAgICAgb2F1dGhTY29wZXMsXG4gICAgICAgIH0sXG4gICAgICBdLFxuICAgIH0pO1xuICB9XG5cbiAgYWRkTm9kZVBvb2wobmFtZTogc3RyaW5nLCBub2RlQ291bnQgPSAzLCBtYWNoaW5lVHlwZSA9IFwiZTItbWVkaXVtXCIpIHtcbiAgICBuZXcgQ29udGFpbmVyTm9kZVBvb2wodGhpcywgbmFtZSwge1xuICAgICAgbmFtZSxcbiAgICAgIGNsdXN0ZXI6IHRoaXMuY2x1c3Rlci5uYW1lLFxuICAgICAgbm9kZUNvdW50LFxuICAgICAgbm9kZUNvbmZpZzogW1xuICAgICAgICB7XG4gICAgICAgICAgcHJlZW1wdGlibGU6IHRydWUsXG4gICAgICAgICAgbWFjaGluZVR5cGUsXG4gICAgICAgICAgc2VydmljZUFjY291bnQ6IHRoaXMuc2EuZW1haWwsXG4gICAgICAgICAgb2F1dGhTY29wZXMsXG4gICAgICAgIH0sXG4gICAgICBdLFxuICAgIH0pO1xuICB9XG5cbiAgYWRkQXV0b3NjYWxpbmdOb2RlUG9vbChcbiAgICBuYW1lOiBzdHJpbmcsXG4gICAgbWluTm9kZUNvdW50ID0gMSxcbiAgICBtYXhOb2RlQ291bnQgPSAyLFxuICAgIG1hY2hpbmVUeXBlID0gXCJlMi1tZWRpdW1cIlxuICApIHtcbiAgICBuZXcgQ29udGFpbmVyTm9kZVBvb2wodGhpcywgbmFtZSwge1xuICAgICAgbmFtZSxcbiAgICAgIGNsdXN0ZXI6IHRoaXMuY2x1c3Rlci5uYW1lLFxuICAgICAgYXV0b3NjYWxpbmc6IFtcbiAgICAgICAge1xuICAgICAgICAgIG1pbk5vZGVDb3VudCxcbiAgICAgICAgICBtYXhOb2RlQ291bnQsXG4gICAgICAgIH0sXG4gICAgICBdLFxuICAgICAgbm9kZUNvbmZpZzogW1xuICAgICAgICB7XG4gICAgICAgICAgcHJlZW1wdGlibGU6IHRydWUsXG4gICAgICAgICAgbWFjaGluZVR5cGUsXG4gICAgICAgICAgc2VydmljZUFjY291bnQ6IHRoaXMuc2EuZW1haWwsXG4gICAgICAgICAgb2F1dGhTY29wZXMsXG4gICAgICAgIH0sXG4gICAgICBdLFxuICAgIH0pO1xuICB9XG5cbiAgc3RhdGljIG9uQ2x1c3RlcihzY29wZTogQ29uc3RydWN0LCBuYW1lOiBzdHJpbmcpIHtcbiAgICBjb25zdCBjbHVzdGVyID0gbmV3IERhdGFHb29nbGVDb250YWluZXJDbHVzdGVyKHNjb3BlLCBcImNsdXN0ZXJcIiwge1xuICAgICAgbmFtZSxcbiAgICB9KTtcblxuICAgIGNvbnN0IGF1dGggPSBuZXcgR0tFQXV0aChzY29wZSwgXCJhdXRoXCIsIHtcbiAgICAgIGNsdXN0ZXJOYW1lOiBjbHVzdGVyLm5hbWUsXG4gICAgICBsb2NhdGlvbjogY2x1c3Rlci5sb2NhdGlvbixcbiAgICAgIHByb2plY3RJZDogY2x1c3Rlci5wcm9qZWN0LFxuICAgIH0pO1xuXG4gICAgbmV3IEt1YmVybmV0ZXNQcm92aWRlcihzY29wZSwgXCJrdWJlcm5ldGVzXCIsIHtcbiAgICAgIGNsdXN0ZXJDYUNlcnRpZmljYXRlOiBhdXRoLmNsdXN0ZXJDYUNlcnRpZmljYXRlT3V0cHV0LFxuICAgICAgaG9zdDogYXV0aC5ob3N0T3V0cHV0LFxuICAgICAgdG9rZW46IGF1dGgudG9rZW5PdXRwdXQsXG4gICAgfSk7XG5cbiAgICByZXR1cm4ge1xuXG4gICAgfTtcbiAgfVxufVxuXG5jbGFzcyBJbmZyYXN0cnVjdHVyZUxheWVyIGV4dGVuZHMgVGVycmFmb3JtU3RhY2sge1xuICBjb25zdHJ1Y3RvcihzY29wZTogQ29uc3RydWN0LCBuYW1lOiBzdHJpbmcpIHtcbiAgICBzdXBlcihzY29wZSwgbmFtZSk7XG5cbiAgICBuZXcgR29vZ2xlUHJvdmlkZXIodGhpcywgXCJnb29nbGVcIiwge1xuICAgICAgem9uZTogXCJ1cy1jZW50cmFsMS1jXCIsXG4gICAgICBwcm9qZWN0OiBcImJpc2hhbC1wbGF5Z3JvdW5kLTMxMzYwMlwiLFxuICAgICAgY3JlZGVudGlhbHM6IEpTT04uc3RyaW5naWZ5KHNhQ3JlZClcbiAgICB9KTtcblxuICAgIGNvbnN0IHNhID0gbmV3IFNlcnZpY2VBY2NvdW50KHRoaXMsIFwic2FcIiwge1xuICAgICAgICBhY2NvdW50SWQ6IFwic2Vuc2FuZC10ZXJyYWZvcm0tY2RrXCIsXG4gICAgICAgIGRpc3BsYXlOYW1lOiBcInNlbnNhbmQtdGVycmFmb3JtLWNka1wiXG4gICAgfSApO1xuXG5cbiAgICBuZXcgUHJvamVjdElhbU1lbWJlcih0aGlzLCBcInNhLXJvbGUtYmluZGluZ1wiLCB7XG4gICAgICByb2xlOiBcInJvbGVzL3N0b3JhZ2UuYWRtaW5cIixcbiAgICAgIG1lbWJlcjogYHNlcnZpY2VBY2NvdW50OiR7c2EuZW1haWx9YCxcbiAgICB9KTtcblxuXG5cbiAgICBuZXcgQ29udGFpbmVyUmVnaXN0cnkodGhpcywgXCJyZWdpc3RyeVwiLCB7fSk7XG5cbiAgICBjb25zdCBjbHVzdGVyID0gbmV3IEt1YmVybmV0ZXNDbHVzdGVyKHRoaXMsIENMVVNURVJfTkFNRSwgc2EpO1xuICAgIGNsdXN0ZXIuYWRkTm9kZVBvb2woXCJtYWluXCIpO1xuICAgIGNsdXN0ZXIuYWRkQXV0b3NjYWxpbmdOb2RlUG9vbChcIndvcmtsb2Fkc1wiKTtcbiAgfVxufVxuXG5jbGFzcyBCYXNlbGluZUxheWVyIGV4dGVuZHMgVGVycmFmb3JtU3RhY2sge1xuICBjb25zdHJ1Y3RvcihzY29wZTogQ29uc3RydWN0LCBuYW1lOiBzdHJpbmcpIHtcbiAgICBzdXBlcihzY29wZSwgbmFtZSk7XG4gICAgbmV3IEdvb2dsZVByb3ZpZGVyKHRoaXMsIFwiZ29vZ2xlXCIsIHtcbiAgICAgIHpvbmU6IFwidXMtY2VudHJhbDEtY1wiLFxuICAgICAgcHJvamVjdDogXCJiaXNoYWwtcGxheWdyb3VuZC0zMTM2MDJcIixcbiAgICB9KTtcblxuICB9XG59XG5cbmNsYXNzIEFwcGxpY2F0aW9uTGF5ZXIgZXh0ZW5kcyBUZXJyYWZvcm1TdGFjayB7XG4gIGNvbnN0cnVjdG9yKHNjb3BlOiBDb25zdHJ1Y3QsIG5hbWU6IHN0cmluZykge1xuICAgIHN1cGVyKHNjb3BlLCBuYW1lKTtcbiAgICBuZXcgR29vZ2xlUHJvdmlkZXIodGhpcywgXCJnb29nbGVcIiwge1xuICAgICAgem9uZTogXCJ1cy1jZW50cmFsMS1jXCIsXG4gICAgICBwcm9qZWN0OiBcImJpc2hhbC1wbGF5Z3JvdW5kLTMxMzYwMlwiLFxuICAgIH0pO1xuICAgIG5ldyBEb2NrZXJQcm92aWRlcih0aGlzLCBcImRvY2tlclwiLCB7fSk7XG4gICAgXG4gIH1cbn1cblxuY29uc3QgYXBwID0gbmV3IEFwcCgpO1xubmV3IEluZnJhc3RydWN0dXJlTGF5ZXIoYXBwLCBcImluZnJhc3RydWN0dXJlXCIpO1xubmV3IEJhc2VsaW5lTGF5ZXIoYXBwLCBcImJhc2VsaW5lXCIpO1xubmV3IEFwcGxpY2F0aW9uTGF5ZXIoYXBwLCBcImRldmVsb3BtZW50XCIpO1xubmV3IEFwcGxpY2F0aW9uTGF5ZXIoYXBwLCBcInN0YWdpbmdcIik7XG5uZXcgQXBwbGljYXRpb25MYXllcihhcHAsIFwicHJvZHVjdGlvblwiKTtcbmFwcC5zeW50aCgpOyJdfQ==