import { Construct } from "constructs";
import { App, TerraformStack } from "cdktf";
import { DockerProvider } from "./.gen/providers/docker/docker-provider";
import { KubernetesProvider } from "./.gen/providers/kubernetes/kubernetes-provider";

import { CLUSTER_NAME } from "./config";
import { TerraformGoogleModulesKubernetesEngineGoogleModulesAuth as GKEAuth } from "./.gen/modules/terraform-google-modules/kubernetes-engine/google/modules/auth";
import { Resource } from "./.gen/providers/null/resource";
import * as saCred from "./secrets/terraform-sa-key.json";
import {
  ContainerCluster,
  ContainerNodePool,
  ContainerRegistry,
  DataGoogleContainerCluster,
  GoogleProvider,
  ProjectIamMember,
  ServiceAccount,
} from "@cdktf/provider-google";

// https://developers.google.com/identity/protocols/oauth2/scopes
const oauthScopes = [
  "https://www.googleapis.com/auth/devstorage.read_only",
  "https://www.googleapis.com/auth/logging.write",
  "https://www.googleapis.com/auth/monitoring",
  "https://www.googleapis.com/auth/servicecontrol",
  "https://www.googleapis.com/auth/service.management.readonly",
  "https://www.googleapis.com/auth/trace.append",
  "https://www.googleapis.com/auth/cloud-platform",
];

class KubernetesCluster extends Resource {
  private sa: ServiceAccount;
  private cluster: ContainerCluster;

  constructor(scope: Construct, name: string, serviceAccount: ServiceAccount) {
    super(scope, name);

    this.sa = serviceAccount;
    this.cluster = new ContainerCluster(this, "cluster", {
      name,
      removeDefaultNodePool: true,
      initialNodeCount: 1,
      nodeConfig: [
        {
          preemptible: true,
          serviceAccount: this.sa.email,
          oauthScopes,
        },
      ],
    });
  }

  addNodePool(name: string, nodeCount = 3, machineType = "e2-medium") {
    new ContainerNodePool(this, name, {
      name,
      cluster: this.cluster.name,
      nodeCount,
      nodeConfig: [
        {
          preemptible: true,
          machineType,
          serviceAccount: this.sa.email,
          oauthScopes,
        },
      ],
    });
  }

  addAutoscalingNodePool(
    name: string,
    minNodeCount = 1,
    maxNodeCount = 2,
    machineType = "e2-medium"
  ) {
    new ContainerNodePool(this, name, {
      name,
      cluster: this.cluster.name,
      autoscaling: [
        {
          minNodeCount,
          maxNodeCount,
        },
      ],
      nodeConfig: [
        {
          preemptible: true,
          machineType,
          serviceAccount: this.sa.email,
          oauthScopes,
        },
      ],
    });
  }

  static onCluster(scope: Construct, name: string) {
    const cluster = new DataGoogleContainerCluster(scope, "cluster", {
      name,
    });

    const auth = new GKEAuth(scope, "auth", {
      clusterName: cluster.name,
      location: cluster.location,
      projectId: cluster.project,
    });

    new KubernetesProvider(scope, "kubernetes", {
      clusterCaCertificate: auth.clusterCaCertificateOutput,
      host: auth.hostOutput,
      token: auth.tokenOutput,
    });

    return {

    };
  }
}

class InfrastructureLayer extends TerraformStack {
  constructor(scope: Construct, name: string) {
    super(scope, name);

    new GoogleProvider(this, "google", {
      zone: "us-central1-c",
      project: "bishal-playground-313602",
      credentials: JSON.stringify(saCred)
    });

    const sa = new ServiceAccount(this, "sa", {
        accountId: "sensand-terraform-cdk",
        displayName: "sensand-terraform-cdk"
    } );


    new ProjectIamMember(this, "sa-role-binding", {
      role: "roles/storage.admin",
      member: `serviceAccount:${sa.email}`,
    });



    new ContainerRegistry(this, "registry", {});

    const cluster = new KubernetesCluster(this, CLUSTER_NAME, sa);
    cluster.addNodePool("main");
    cluster.addAutoscalingNodePool("workloads");
  }
}

class BaselineLayer extends TerraformStack {
  constructor(scope: Construct, name: string) {
    super(scope, name);
    new GoogleProvider(this, "google", {
      zone: "us-central1-c",
      project: "bishal-playground-313602",
    });

  }
}

class ApplicationLayer extends TerraformStack {
  constructor(scope: Construct, name: string) {
    super(scope, name);
    new GoogleProvider(this, "google", {
      zone: "us-central1-c",
      project: "bishal-playground-313602",
    });
    new DockerProvider(this, "docker", {});
    
  }
}

const app = new App();
new InfrastructureLayer(app, "infrastructure");
new BaselineLayer(app, "baseline");
new ApplicationLayer(app, "development");
new ApplicationLayer(app, "staging");
new ApplicationLayer(app, "production");
app.synth();