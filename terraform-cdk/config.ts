export const VERSION = process.env.VERSION || "latest";
export const DOCKER_ORG = "sensand-cdk-test";
export const CLUSTER_NAME = "cluster";