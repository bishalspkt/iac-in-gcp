import { Construct } from "constructs";
import { Resource } from "./.gen/providers/null/resource";
export declare function buildAndPushImage(scope: Construct, imageName: string, p: string): [string, Resource];
