"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildAndPushImage = void 0;
const cdktf_1 = require("cdktf");
const fs = require("fs");
const path = require("path");
const config_1 = require("./config");
const resource_1 = require("./.gen/providers/null/resource");
const provider_google_1 = require("@cdktf/provider-google");
function buildAndPushImage(scope, imageName, p) {
    const _ = (name) => `${imageName}-${name}`;
    const files = fs.readdirSync(p);
    function getDockerfileFlag() {
        if (files.includes("Dockerfile")) {
            return "";
        }
        if (files.includes("package.json")) {
            const asset = new cdktf_1.TerraformAsset(scope, _("node-dockerfile"), {
                path: path.resolve(__dirname, "Dockerfile.node"),
            });
            return `-f ${asset.path}`;
        }
        if (files.includes("Cargo.toml")) {
            const asset = new cdktf_1.TerraformAsset(scope, _("node-dockerfile"), {
                path: path.resolve(__dirname, "Dockerfile.rust"),
            });
            return `-f ${asset.path}`;
        }
        throw new Error("Unknown application language, please add a Dockerfile or use node or rust");
    }
    function getVersion() {
        if (files.includes("package.json")) {
            return require(path.resolve(p, "package.json")).version;
        }
        return config_1.VERSION;
    }
    const dockerfileFlag = getDockerfileFlag();
    const content = new cdktf_1.TerraformAsset(scope, _("content"), {
        path: p,
    });
    const sa = new provider_google_1.DataGoogleServiceAccount(scope, _("sa"), {
        accountId: "registry-push",
    });
    const key = new provider_google_1.ServiceAccountKey(scope, _("sa-key"), {
        serviceAccountId: sa.email,
    });
    const version = getVersion();
    const tag = `gcr.io/${config_1.DOCKER_ORG}/${imageName}:${version}-${content.assetHash}`;
    const image = new resource_1.Resource(scope, _("image"), {
        triggers: {
            tag,
        },
    });
    const cmd = `echo '${key.privateKey}' | base64 -D | docker login -u _json_key --password-stdin https://gcr.io && docker build ${dockerfileFlag} -t ${tag} ${content.path} && docker push ${tag}`;
    image.addOverride("provisioner.local-exec.command", cmd);
    return [tag, image];
}
exports.buildAndPushImage = buildAndPushImage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9ja2VyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZG9ja2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLGlDQUF1QztBQUV2Qyx5QkFBeUI7QUFDekIsNkJBQTZCO0FBQzdCLHFDQUErQztBQUMvQyw2REFBMEQ7QUFDMUQsNERBR2dDO0FBRWhDLFNBQWdCLGlCQUFpQixDQUMvQixLQUFnQixFQUNoQixTQUFpQixFQUNqQixDQUFTO0lBRVQsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFZLEVBQUUsRUFBRSxDQUFDLEdBQUcsU0FBUyxJQUFJLElBQUksRUFBRSxDQUFDO0lBQ25ELE1BQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFaEMsU0FBUyxpQkFBaUI7UUFDeEIsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQ2hDLE9BQU8sRUFBRSxDQUFDO1NBQ1g7UUFFRCxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEVBQUU7WUFDbEMsTUFBTSxLQUFLLEdBQUcsSUFBSSxzQkFBYyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsaUJBQWlCLENBQUMsRUFBRTtnQkFDNUQsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLGlCQUFpQixDQUFDO2FBQ2pELENBQUMsQ0FBQztZQUVILE9BQU8sTUFBTSxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDM0I7UUFFRCxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDaEMsTUFBTSxLQUFLLEdBQUcsSUFBSSxzQkFBYyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsaUJBQWlCLENBQUMsRUFBRTtnQkFDNUQsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLGlCQUFpQixDQUFDO2FBQ2pELENBQUMsQ0FBQztZQUVILE9BQU8sTUFBTSxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDM0I7UUFFRCxNQUFNLElBQUksS0FBSyxDQUNiLDJFQUEyRSxDQUM1RSxDQUFDO0lBQ0osQ0FBQztJQUVELFNBQVMsVUFBVTtRQUNqQixJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEVBQUU7WUFDbEMsT0FBTyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7U0FDekQ7UUFFRCxPQUFPLGdCQUFPLENBQUM7SUFDakIsQ0FBQztJQUVELE1BQU0sY0FBYyxHQUFHLGlCQUFpQixFQUFFLENBQUM7SUFDM0MsTUFBTSxPQUFPLEdBQUcsSUFBSSxzQkFBYyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLEVBQUU7UUFDdEQsSUFBSSxFQUFFLENBQUM7S0FDUixDQUFDLENBQUM7SUFFSCxNQUFNLEVBQUUsR0FBRyxJQUFJLDBDQUF3QixDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUU7UUFDdEQsU0FBUyxFQUFFLGVBQWU7S0FDM0IsQ0FBQyxDQUFDO0lBRUgsTUFBTSxHQUFHLEdBQUcsSUFBSSxtQ0FBaUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1FBQ3BELGdCQUFnQixFQUFFLEVBQUUsQ0FBQyxLQUFLO0tBQzNCLENBQUMsQ0FBQztJQUVILE1BQU0sT0FBTyxHQUFHLFVBQVUsRUFBRSxDQUFDO0lBRTdCLE1BQU0sR0FBRyxHQUFHLFVBQVUsbUJBQVUsSUFBSSxTQUFTLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNoRixNQUFNLEtBQUssR0FBRyxJQUFJLG1CQUFRLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRTtRQUM1QyxRQUFRLEVBQUU7WUFDUixHQUFHO1NBQ0o7S0FDRixDQUFDLENBQUM7SUFFSCxNQUFNLEdBQUcsR0FBRyxTQUFTLEdBQUcsQ0FBQyxVQUFVLDZGQUE2RixjQUFjLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBQyxJQUFJLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztJQUNqTSxLQUFLLENBQUMsV0FBVyxDQUFDLGdDQUFnQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBRXpELE9BQU8sQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7QUFDdEIsQ0FBQztBQXBFRCw4Q0FvRUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUZXJyYWZvcm1Bc3NldCB9IGZyb20gXCJjZGt0ZlwiO1xuaW1wb3J0IHsgQ29uc3RydWN0IH0gZnJvbSBcImNvbnN0cnVjdHNcIjtcbmltcG9ydCAqIGFzIGZzIGZyb20gXCJmc1wiO1xuaW1wb3J0ICogYXMgcGF0aCBmcm9tIFwicGF0aFwiO1xuaW1wb3J0IHsgVkVSU0lPTiwgRE9DS0VSX09SRyB9IGZyb20gXCIuL2NvbmZpZ1wiO1xuaW1wb3J0IHsgUmVzb3VyY2UgfSBmcm9tIFwiLi8uZ2VuL3Byb3ZpZGVycy9udWxsL3Jlc291cmNlXCI7XG5pbXBvcnQge1xuICBEYXRhR29vZ2xlU2VydmljZUFjY291bnQsXG4gIFNlcnZpY2VBY2NvdW50S2V5LFxufSBmcm9tIFwiQGNka3RmL3Byb3ZpZGVyLWdvb2dsZVwiO1xuXG5leHBvcnQgZnVuY3Rpb24gYnVpbGRBbmRQdXNoSW1hZ2UoXG4gIHNjb3BlOiBDb25zdHJ1Y3QsXG4gIGltYWdlTmFtZTogc3RyaW5nLFxuICBwOiBzdHJpbmdcbik6IFtzdHJpbmcsIFJlc291cmNlXSB7XG4gIGNvbnN0IF8gPSAobmFtZTogc3RyaW5nKSA9PiBgJHtpbWFnZU5hbWV9LSR7bmFtZX1gO1xuICBjb25zdCBmaWxlcyA9IGZzLnJlYWRkaXJTeW5jKHApO1xuXG4gIGZ1bmN0aW9uIGdldERvY2tlcmZpbGVGbGFnKCkge1xuICAgIGlmIChmaWxlcy5pbmNsdWRlcyhcIkRvY2tlcmZpbGVcIikpIHtcbiAgICAgIHJldHVybiBcIlwiO1xuICAgIH1cblxuICAgIGlmIChmaWxlcy5pbmNsdWRlcyhcInBhY2thZ2UuanNvblwiKSkge1xuICAgICAgY29uc3QgYXNzZXQgPSBuZXcgVGVycmFmb3JtQXNzZXQoc2NvcGUsIF8oXCJub2RlLWRvY2tlcmZpbGVcIiksIHtcbiAgICAgICAgcGF0aDogcGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgXCJEb2NrZXJmaWxlLm5vZGVcIiksXG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIGAtZiAke2Fzc2V0LnBhdGh9YDtcbiAgICB9XG5cbiAgICBpZiAoZmlsZXMuaW5jbHVkZXMoXCJDYXJnby50b21sXCIpKSB7XG4gICAgICBjb25zdCBhc3NldCA9IG5ldyBUZXJyYWZvcm1Bc3NldChzY29wZSwgXyhcIm5vZGUtZG9ja2VyZmlsZVwiKSwge1xuICAgICAgICBwYXRoOiBwYXRoLnJlc29sdmUoX19kaXJuYW1lLCBcIkRvY2tlcmZpbGUucnVzdFwiKSxcbiAgICAgIH0pO1xuXG4gICAgICByZXR1cm4gYC1mICR7YXNzZXQucGF0aH1gO1xuICAgIH1cblxuICAgIHRocm93IG5ldyBFcnJvcihcbiAgICAgIFwiVW5rbm93biBhcHBsaWNhdGlvbiBsYW5ndWFnZSwgcGxlYXNlIGFkZCBhIERvY2tlcmZpbGUgb3IgdXNlIG5vZGUgb3IgcnVzdFwiXG4gICAgKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGdldFZlcnNpb24oKTogc3RyaW5nIHtcbiAgICBpZiAoZmlsZXMuaW5jbHVkZXMoXCJwYWNrYWdlLmpzb25cIikpIHtcbiAgICAgIHJldHVybiByZXF1aXJlKHBhdGgucmVzb2x2ZShwLCBcInBhY2thZ2UuanNvblwiKSkudmVyc2lvbjtcbiAgICB9XG5cbiAgICByZXR1cm4gVkVSU0lPTjtcbiAgfVxuXG4gIGNvbnN0IGRvY2tlcmZpbGVGbGFnID0gZ2V0RG9ja2VyZmlsZUZsYWcoKTtcbiAgY29uc3QgY29udGVudCA9IG5ldyBUZXJyYWZvcm1Bc3NldChzY29wZSwgXyhcImNvbnRlbnRcIiksIHtcbiAgICBwYXRoOiBwLFxuICB9KTtcblxuICBjb25zdCBzYSA9IG5ldyBEYXRhR29vZ2xlU2VydmljZUFjY291bnQoc2NvcGUsIF8oXCJzYVwiKSwge1xuICAgIGFjY291bnRJZDogXCJyZWdpc3RyeS1wdXNoXCIsXG4gIH0pO1xuXG4gIGNvbnN0IGtleSA9IG5ldyBTZXJ2aWNlQWNjb3VudEtleShzY29wZSwgXyhcInNhLWtleVwiKSwge1xuICAgIHNlcnZpY2VBY2NvdW50SWQ6IHNhLmVtYWlsLFxuICB9KTtcblxuICBjb25zdCB2ZXJzaW9uID0gZ2V0VmVyc2lvbigpO1xuXG4gIGNvbnN0IHRhZyA9IGBnY3IuaW8vJHtET0NLRVJfT1JHfS8ke2ltYWdlTmFtZX06JHt2ZXJzaW9ufS0ke2NvbnRlbnQuYXNzZXRIYXNofWA7XG4gIGNvbnN0IGltYWdlID0gbmV3IFJlc291cmNlKHNjb3BlLCBfKFwiaW1hZ2VcIiksIHtcbiAgICB0cmlnZ2Vyczoge1xuICAgICAgdGFnLFxuICAgIH0sXG4gIH0pO1xuXG4gIGNvbnN0IGNtZCA9IGBlY2hvICcke2tleS5wcml2YXRlS2V5fScgfCBiYXNlNjQgLUQgfCBkb2NrZXIgbG9naW4gLXUgX2pzb25fa2V5IC0tcGFzc3dvcmQtc3RkaW4gaHR0cHM6Ly9nY3IuaW8gJiYgZG9ja2VyIGJ1aWxkICR7ZG9ja2VyZmlsZUZsYWd9IC10ICR7dGFnfSAke2NvbnRlbnQucGF0aH0gJiYgZG9ja2VyIHB1c2ggJHt0YWd9YDtcbiAgaW1hZ2UuYWRkT3ZlcnJpZGUoXCJwcm92aXNpb25lci5sb2NhbC1leGVjLmNvbW1hbmRcIiwgY21kKTtcblxuICByZXR1cm4gW3RhZywgaW1hZ2VdO1xufSJdfQ==