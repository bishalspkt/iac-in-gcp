# Terraform CDK (Typescript)
This spike uses Terraform CDK to provision a Google Kubernetes Engine resource.

*Note: Terraform CDK allows developers to use familiar programming languages to define cloud inrastructure. Terraform CDK then generates HashiCorp Configuration Language to provision it through Terraform.

[Refernce](https://github.com/hashicorp/terraform-cdk/tree/main/examples/typescript/google)

## How to Use