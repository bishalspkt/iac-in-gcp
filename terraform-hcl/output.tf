output "cluster_name" {
  description = "Cluster name"
  value       = module.gke.name
}

output "ca_certificate" {
  description = "Cluster name"
  value       = module.gke.ca_certificate
  sensitive = true
}
output "endpoint" {
  description = "Cluster name"
  value       = module.gke.endpoint
  sensitive = true
}