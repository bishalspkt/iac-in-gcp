# Terraform (HCL)
This spike uses Terraform's Hashicorp Configuration Language to provision a Google Kubernetes Engine resource. 

## How to Use
In order to provision resources, Terraform's google provider needs to be configured with your `GCP_PROJECT_ID`, and your [Google Cloud service account key file](https://cloud.google.com/iam/docs/creating-managing-service-account-keys). 

To set these configuration items easily, you can create a `terraform.tfvars` file with the following contents

```
project = "GCP_PROJECT_ID"
credentials_file = "PATH_TO_SERVICE_ACC_KEY_JSON"
region = "REGION"
zone = "ZONE"
```

**If you are trying Terraform on a new GCP Project, remember to enable APIs for Googke Kubernetes Engine, and other as required.**


Once you have set the required configuration items, to apply your resources, run the follwing

```bash
# Initialize your terraform project. This genrates `.lock.hcl` file and `.terraform/` directory locally
terraform init

# Format and validate your terraform configuration
terraform fmt && terraform validate

# Plan your resource migration
terraform plan

# Finally, apply your terraform plan
terraform apply

# Inspect current state using
terraform show

terraform output

# To delete the deployed resources
terraform destroy
```