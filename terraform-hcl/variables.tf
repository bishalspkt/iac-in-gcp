variable "credentials_file" {
  description = "The credentials file to connect with gcloud"
}
variable "project_id" {
  description = "The project ID to host the cluster in"
}
variable "cluster_name" {
  description = "The name for the GKE cluster"
  default     = "sensand-hcl-cluster"
}
variable "env_name" {
  description = "The environment for the GKE cluster"
  default     = "prod"
}
variable "region" {
  description = "The region to host the cluster in"
}
variable "zone" {
  description = "The zone to host the cluster in"
}
variable "network" {
  description = "The VPC network created to host the cluster in"
  default     = "sensand-hcl-gke-network"
}
variable "subnetwork" {
  description = "The subnetwork created to host the cluster in"
  default     = "sensand-hcl-gke-subnet"
}
variable "ip_range_pods_name" {
  description = "The secondary ip range to use for pods"
  default     = "sensand-hcl-gke-ip-range-pods"
}
variable "ip_range_services_name" {
  description = "The secondary ip range to use for services"
  default     = "sensand-hcl-gke-ip-range-services"
}