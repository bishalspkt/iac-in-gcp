# Infrastructure as Code in Google Cloud Platform
In this spike, we explore 3 tools for provisioning infrastructure in Google Cloud Platform using declarative configuration. The tools explored are:
- [Terraform HCL(Hashicorp Configuration Language](./terraform-hcl)
- [Terraform CDK(Using Typescript)](./terraform-cdk)
- [Google Cloud Deployment Manager](./deployment-manager)

As a simple test, a Google Kubernetes Engine cluster is provisioned using each of these 3 methods. In order to run these spikes against your own Google Cloud, you will need the following:
- Your target `GCP_PROJECT_ID`
- A `service-account-credentials.json` file for authentication with GCP.
- Your preferred GCP `REGION`
- Your preferref GCP `ZONE`
