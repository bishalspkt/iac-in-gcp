# Google Cloud Deployment Manager
This spike uses Google Cloud Deployment Manager to provision a Google Kubernetes Engine resource.

## How to Use

```bash
NAME=cluster-name
PROJECT=gcp-project-id

# Deploy using deployment manager. Needs the Deployment Manager API to be activated
gcloud deployment-manager deployments create $NAME --config=gke.yaml --project=$PROJECT
```